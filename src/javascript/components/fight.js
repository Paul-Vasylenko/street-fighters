import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  //health bar of player 1, 100%
  const firstFighterHealth = document.querySelector(".arena___health-bar#left-fighter-indicator");
  firstFighterHealth.style.width="100%";
  //health bar of player 2, 100%
  const secondFighterHealth = document.querySelector(".arena___health-bar#right-fighter-indicator");
  secondFighterHealth.style.width="100%";
  //number health player1
  let healthNumber1 = firstFighter.health;
  //number health player2
  let healthNumber2 = secondFighter.health;
  //criticalHit last used timing player1
  let lastUsed1;
  //criticalHit last used timing player2
  let lastUsed2;
  //answer to question: was critical hit already used? player1
  let criticalHitWasUsed1 = false;
  //answer to question: was critical hit already used? player2
  let criticalHitWasUsed2 = false;
  //set of information about pressed buttons.
  let pressedButtonsSet = new Set();

  return new Promise((resolve) => {
    document.addEventListener('keydown', (event)=>{
      //On block buttons and combination-buttons put to set. That's how I know if 1 or more buttons are pressed together
      switch(event.code){
        case controls.PlayerTwoBlock:
            pressedButtonsSet.add(controls.PlayerTwoBlock);
            break;
        case controls.PlayerOneBlock:
            pressedButtonsSet.add(controls.PlayerOneBlock);
            break;
        case controls.PlayerOneCriticalHitCombination[0]:
            pressedButtonsSet.add(controls.PlayerOneCriticalHitCombination[0]);
            break;
        case controls.PlayerOneCriticalHitCombination[1]:
            pressedButtonsSet.add(controls.PlayerOneCriticalHitCombination[1]);
            break;
        case controls.PlayerOneCriticalHitCombination[2]:
            pressedButtonsSet.add(controls.PlayerOneCriticalHitCombination[2]);
            break;
        case controls.PlayerTwoCriticalHitCombination[0]:
            pressedButtonsSet.add(controls.PlayerTwoCriticalHitCombination[0]);
            break;
        case controls.PlayerTwoCriticalHitCombination[1]:
            pressedButtonsSet.add(controls.PlayerTwoCriticalHitCombination[1]);
            break;
        case controls.PlayerTwoCriticalHitCombination[2]:
            pressedButtonsSet.add(controls.PlayerTwoCriticalHitCombination[2]);
            break;
      }
      //Player 1 attacks and he doesn't block as well
      if(event.code == controls.PlayerOneAttack && !pressedButtonsSet.has(controls.PlayerOneBlock)){
        let damage;
        //Player 1 attacks, but player 2 blocks
        if(pressedButtonsSet.has(controls.PlayerTwoBlock)){
          damage = 0;
        }
        //Player 1 attacks, and player 2 doesn't block
        else{
          damage = +((getDamage(firstFighter,secondFighter)).toFixed(2));
        }
        healthNumber2-=damage; // health left player2
        if(healthNumber2>0){
          //e.g.
          //45 - 100%
          //40 - x      => x=(40*100/45).toFixed(2)+"%" => x="88.80%"
          secondFighterHealth.style.width = ((healthNumber2*100)/secondFighter.health).toFixed(2) + "%";
        }
        else{
          //player1 win
          secondFighterHealth.style.width = "0%";
          resolve(firstFighter);
        }
      }
      //player 1 criticalHit QWE.  Can't be blocked and = 2 * attack 
      if(pressedButtonsSet.has(controls.PlayerOneCriticalHitCombination[0]) &&
         pressedButtonsSet.has(controls.PlayerOneCriticalHitCombination[1]) &&
         pressedButtonsSet.has(controls.PlayerOneCriticalHitCombination[2]) &&
         !pressedButtonsSet.has(controls.PlayerOneBlock)){
           let damage=0;
           //if it is the first time we use QWE
           if(criticalHitWasUsed1 == false){
            damage = firstFighter.attack * 2;
            lastUsed1 = new Date();
            criticalHitWasUsed1=true;
           }
           //if 10 seconds lasted
           else{
             let now = new Date(); //when used
             if(+now-lastUsed1>10000){//if last use was 10 seconds ago
              damage = firstFighter.attack * 2;
              lastUsed1 = now;
            }
           }
           healthNumber2-=damage; // health left player1
           if(healthNumber2>0){
             secondFighterHealth.style.width = ((healthNumber2*100)/secondFighter.health).toFixed(2) + "%";
           }
           else{
            secondFighterHealth.style.width = "0%"
            resolve(firstFighter);
           }
         }

         //Second player
          //Player 2 attacks and he doesn't block as well
         if(event.code == controls.PlayerTwoAttack && !pressedButtonsSet.has(controls.PlayerTwoBlock)){
          let damage;
          //Player 2 attacks, but player 1 blocks
          if(pressedButtonsSet.has(controls.PlayerOneBlock)){
            damage = 0;
          }
          //Player 2 attacks, and player 1 doesn't block
          else{
            damage = +((getDamage(secondFighter,firstFighter)).toFixed(2));
          }
          healthNumber1-=damage; // health left player1
          if(healthNumber1>0){
            firstFighterHealth.style.width = ((healthNumber1*100)/firstFighter.health ).toFixed(2)+ "%";
          }
          else{
            //player2 win
            firstFighterHealth.style.width = "0%";
            resolve(secondFighter);
          }
        }

        //player 2 criticalHit UIO.  Can't be blocked and = 2 * attack 
        if(pressedButtonsSet.has(controls.PlayerTwoCriticalHitCombination[0]) &&
           pressedButtonsSet.has(controls.PlayerTwoCriticalHitCombination[1]) &&
           pressedButtonsSet.has(controls.PlayerTwoCriticalHitCombination[2]) &&
           !pressedButtonsSet.has(controls.PlayerTwoBlock) ){
             let damage=0;
             //if it is the first time we use UIO
             if(criticalHitWasUsed2 == false){
              damage = secondFighter.attack * 2;
              lastUsed2 = new Date();
              criticalHitWasUsed2=true;
             }
             //if 10 seconds lasted
             else{
               let now = new Date(); //when used
               if(+now-lastUsed2>10000){//if last use was 10 seconds ago
                damage = firstFighter.attack * 2;
                lastUsed2 = now;
              }
             }
             healthNumber1-=damage; // health left player1
             if(healthNumber1>0){
               firstFighterHealth.style.width = ((healthNumber1*100)/firstFighter.health).toFixed(2) + "%";
             }
             else{
              firstFighterHealth.style.width = "0%";
              resolve(secondFighter);
             }
           }
    })


    //On keyup delete button from Set
    document.addEventListener('keyup', (event)=>{
      switch(event.code){
        case controls.PlayerTwoBlock:
            pressedButtonsSet.delete(controls.PlayerTwoBlock);
            break;
        case controls.PlayerOneBlock:
            pressedButtonsSet.delete(controls.PlayerOneBlock);
            break;
        case controls.PlayerOneCriticalHitCombination[0]:
            pressedButtonsSet.delete(controls.PlayerOneCriticalHitCombination[0]);
            break;
        case controls.PlayerOneCriticalHitCombination[1]:
            pressedButtonsSet.delete(controls.PlayerOneCriticalHitCombination[1]);
            break;
        case controls.PlayerOneCriticalHitCombination[2]:
            pressedButtonsSet.delete(controls.PlayerOneCriticalHitCombination[2]);
            break;
        case controls.PlayerTwoCriticalHitCombination[0]:
            pressedButtonsSet.delete(controls.PlayerTwoCriticalHitCombination[0]);
            break;
        case controls.PlayerTwoCriticalHitCombination[1]:
            pressedButtonsSet.delete(controls.PlayerTwoCriticalHitCombination[1]);
            break;
        case controls.PlayerTwoCriticalHitCombination[2]:
            pressedButtonsSet.delete(controls.PlayerTwoCriticalHitCombination[2]);
            break;
      }
    })
  });
}

export function getDamage(attacker, defender) {
  const diff= getHitPower(attacker) - getBlockPower(defender);
  if(diff>0){
    return diff;
  }else{
    return 0;
  }
  // return damage
}

export function getHitPower(fighter) {
  const attack = fighter.attack;
  const randomNumber = getRandom();
  const power = randomNumber * attack;
  return power
  // return hit power
}

export function getBlockPower(fighter) {
  const defense = fighter.defense;
  const randomNumber = getRandom();
  const power =  randomNumber * defense;
  return power
  // return block power
}

function getRandom(){
  //random number from 1 to 2
  const number = Math.random()+1;
  return number;
}