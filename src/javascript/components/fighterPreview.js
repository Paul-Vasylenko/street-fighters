import { createElement } from '../helpers/domHelper';
import {showModal} from './modal/modal';
const rootElement = document.getElementById('root');

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if(fighter){
    const fighterImg = createFighterImage(fighter);
    if(position=='right'){
      fighterImg.style.transform = "scaleX(-1)";
    }
    if(fighter._id==1){
      fighterImg.style.maxHeight = "400px";
    }

    let detailsBlock = createElement({
      tagName: 'div',
      className: 'details'
    }) 
    let detailsText = createElement({
          tagName: 'div',
          className:'details-text'
        });

        detailsText.innerHTML =  '<p><br>Health: '+fighter.health +'<br>Attack: '+fighter.attack+'<br>Defense : '+fighter.defense+'</p>';
   detailsBlock.append(detailsText)
    fighterElement.append(fighterImg, detailsBlock);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
