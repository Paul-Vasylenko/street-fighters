import {showModal} from './modal';


export function showWinnerModal(fighter) {
  showModal({title: `WINNER - ${fighter.name}`, bodyElement: "You won this one!", onClose: () => {
    document.getElementById('root').innerHTML='';
    document.location.reload();
  }
  })
  // call showModal function 
}
